import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Supplierlogin from '../Login/supplierlogin'


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: 'url(https://cdn2.hubspot.net/hubfs/4483341/Imported_Blog_Media/shutterstock_252243379-1024x683.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
export default function SignInSide() {
    const classes = useStyles();
     const adminUser ={
      email:"manosankart94@gmail.com",
      password:"admin@123"
    }
    
    const[user, setUser]=useState({email:"", password:""});
    const[error, setError]=useState("");

    const Login = details => {
      console.log(details, "111111")
      if(details.email == adminUser.email && details.password == adminUser.password){
      console.log("loggedin");
      setUser({
        email: details.email,
        password: details.password
      });
      }else 
      {
        setError("Details Does not Match")
      }
    }
    const Logout =() =>{
             setUser({ name:"", email:""})
    }
  return (
    <div>
    {(user.email !="") ? (
      <div>
      <h2>welcome</h2><span>{user.email}</span>
      <button onClick={Logout}>Logout</button>
      </div>
    ):(
      <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
      <Supplierlogin Login={Login} error={error}/>
      </div>
      </Grid>
    </Grid>
    )}
   </div>
    
  );
}