import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import SearchBar from "material-ui-search-bar";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import '../components/navbarstyle.css';
import DashboardIcon from '@material-ui/icons/Dashboard';
import Badge from '@material-ui/core/Badge';
import { withStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';
import SystemUpdateAltIcon from '@material-ui/icons/SystemUpdateAlt';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import BarChartIcon from '@material-ui/icons/BarChart';
import Purchaseorder from '../components/Purchaseorder/purchaseorder'
import StorefrontIcon from '@material-ui/icons/Storefront';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
// import Createpo from '../Pages/Purchaseordercommon/Purchaseorder/Createpo';
// import Orderdetail from '../Pages/Purchaseordercommon/Purchaseorder/orderdetail'
import Avatar from '@material-ui/core/Avatar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
// import Invoice from '../pages/Purchaseordercommon/Invoice/Ivoice'
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import LockIcon from '@material-ui/icons/Lock';

const drawerWidth = 240;

const menus = [
  {
    label: "Home",
    url: "/",
    icon: <HomeIcon />,
  },
  {
    label: "Products",
    url: "/Products",
    icon: <SystemUpdateAltIcon />,
    submenu: [
      {
        label: "Sub Menu 1"
      },
      {
        label: "Sub Menu 2"
      }
    ]
  },
  {
    label: "Supplier",
    url: "/Supplier",
    icon: <HomeIcon />,
  },
]

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },


  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },


}));

export default function MiniDrawer() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
 
  const handleDrawerOpen = () => {
    setOpen(open === "false" ? "true" : "true");
  };
  console.log(open)

  const handleDrawerClose = () => {
    setOpen(true);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
        <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          {open == false ? <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            className={classes.menuButton}
          >
            <MenuIcon
              classes={{
                root: open
                  ? classes.menuButtonIconOpen
                  : classes.menuButtonIconClosed
              }}
            />
          </IconButton> : <IconButton color="inherit" aria-label="Open drawer" onClick={handleDrawerClose}
            className={classes.menuButton}><MenuIcon /> </IconButton>
          }
          <Typography variant="h6" noWrap>
            <div><img src="https://s1.poorvikamobile.com/image/data/logo/poorvika-logo-white.png" style={{ width: '135px', marginTop: '9px' }} />
            </div>
          </Typography>
          <SearchBar className="searchbar"
            onChange={() => console.log('onChange')}
            onRequestSearch={() => console.log('onRequestSearch')}
            style={{
              margin: '0 auto',
              width: '500px',
              height: '32px', marginLeft: '180px', background: 'rgb(255 255 255 / 84%)',
            }}
          />

          
                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}/>
                <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}><AccountCircleIcon color="primary"/>&nbsp; Profile</MenuItem>
        <MenuItem onClick={handleClose}><AccountBalanceIcon color="primary"/>&nbsp; My account</MenuItem>
        <MenuItem ><LockIcon color="primary"/>Logout</MenuItem>
      </Menu>
        </Toolbar>
      </AppBar>
      <Drawer  variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
        open={open}>

        <Divider />
        <List style={{paddingTop:'64px!important'}}>
          {menus.map((text, index) => (
            <Link to={text.url} className="router-link"><ListItem button key={text}>
              <ListItemIcon style={{ color: '#999' }}>{text.icon}</ListItemIcon>
              <ListItemText >{text.label}</ListItemText>
            </ListItem></Link>
          ))}
        </List>
        <Divider />
        <List>
          <ListItem style={{ fontWeight: '600' }}>SALES CHANNELS</ListItem>
          <ListItem >
            <ListItemIcon style={{ color: '#999' }}> <StorefrontIcon /></ListItemIcon>
            <ListItemText>Online Store</ListItemText>
          </ListItem>
          <ListItem >
            <ListItemIcon style={{ color: '#999' }}> <LocalMallIcon /></ListItemIcon>
            <a href="https://auth.poorvika.com/auth/realms/poorvika/protocol/openid-connect/auth?client_id=pos&redirect_uri=https%3A%2F%2Fpos.poorvika.com%2Flogin&state=4dbfe04c-08cb-4a21-93d1-e93d3d63f10e&response_mode=fragment&response_type=code&scope=openid&nonce=4c763d38-05e3-4d21-97bf-39d82dfa3d0c"><ListItemText>Point Of Sale</ListItemText></a>
          </ListItem>
        </List>
      </Drawer>
    </div>
  );
}
