import React from 'react'
import Grid from '@material-ui/core/Grid';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import '../Invoice/Invoicestyle.css'
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Navbar from '../navbar'
import Profileimg from '../assets/profile_image.jpeg'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

export default function Orderdetail() {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className="main-common">
      <Navbar />
    <Paper>
        <Grid container spacing={3} >
          <Grid item md={9} xs={6} className="breadcrumbs">
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="/" >Simple Purchase Orders</Link>
              {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}></Link> */}
              <Typography color="textPrimary">Welcome</Typography>
            </Breadcrumbs>
          </Grid>
          <Grid item md={3} xs={6} className="breadcrumbs1">
            <Button variant="outlined" size="small">Resend Po</Button>
            <Button variant="contained" size="small" color="primary">Edit</Button>
          </Grid>
        </Grid>
      </Paper>
      <div style={{ width: '100%', marginTop: '18px' }}>
        <Paper>
        <Grid container spacing={3} style={{marginBottom:'2px'}}>
        <Grid item xs={12} align="center" fontWeight="fontWeightBold">Invoice Details</Grid>
      </Grid>
      <hr className="hr-line"/>
          <Grid container spacing={3} style={{textAlign:'center', marginBottom:'10px'}}>
<Grid item md={4} xs={6}>Invoice Number: 1030</Grid>
<Grid item md={3} xs={6}>Date: 2020-04-20</Grid>
<Grid item md={5} xs={12}><ButtonGroup size="small" aria-label="small outlined button group">
                  <Button className="common-button" variant="contained" color="primary">Mark Completed</Button>
                  <Button className="common-button" variant="contained" color="primary">Transfer All</Button>
                  <Button>Delete</Button>
                </ButtonGroup>
                </Grid>
</Grid>
          <TableContainer >
            <Table aria-label="simple table">
              <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
                <TableRow className="orderdetails-table">
                  <TableCell align="center" style={{ width: '35%' }}>Item</TableCell>
                  <TableCell align="center" style={{ width: '30%' }}>Quantity</TableCell>
                  <TableCell align="center" style={{ width: '35%' }}>Transfer</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow className="orderdetails-table1">
                  <TableCell align="center">12T5KI - 12 Ti Xelium Ski"s - 163cm</TableCell>
                  <TableCell align="center">1</TableCell>
                  <TableCell align="center" variant="outlined" color="primary" onClick={handleClickOpen}>Update Inventory</TableCell>
                </TableRow>
                <TableRow className="orderdetails-table1">
                  <TableCell align="center">12T5KI - 12 Ti Xelium Ski"s - 163cm</TableCell>
                  <TableCell align="center">1</TableCell>
                  <TableCell align="center" variant="outlined" color="primary" onClick={handleClickOpen}>Update Inventory</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>

      {/* Popup Modal */}
      <div>

        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Are you sure you want to transfer this?"}</DialogTitle>
          <hr />
          <DialogContent>
            <DialogContentText>
              This will transfer of 12T5KI - 12 Ti Xelium Ski"s - 163cm
          </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleClose} variant="contained" size="small">Cancel</Button>
            <Button onClick={handleClose} color="primary" variant="contained" size="small" autoFocus>Transfer</Button>
          </DialogActions>
        </Dialog>
      </div>


<Paper className="invoice-print">
<Grid container spacing={3} >
          <Grid item md={4} className="font-weight">
             <Typography className="">Billing Address</Typography>
             <p>Poorvika Head Office</p>
             <p>No:32, AGR Platina, 3rd Main Road,</p>
             <p>Kalaimagal Nagar, Ekkatuthangal,</p>
             <p>Chennai, Tamil Nadu 600032</p>  
             </Grid>
          <Grid item md={4} className="font-weight" style={{textAlign:'center'}}><Typography>Purchase Order</Typography>  
          <p>GHOST1031</p>
             <p>PO Date 2020-04-20</p>
              </Grid>
          <Grid item md={4} className="font-weight" style={{textAlign:'center'}}><img src={Profileimg} style={{ width: '125px' }}></img>  </Grid>
        </Grid>
        <Grid container spacing={3}  style={{marginTop:'50px'}}>
          <Grid item md={4} className="font-weight">
             <Typography>Shipping To</Typography>
             <p>Poorvika Mobiles Pvt Ltd</p>
             <p>No. 82, Arcot Road, Anna Valaagam Opp: ICICI Bank,</p>
             <p> Kodambakkam,</p>
             <p>Chennai, Tamil Nadu 600024</p>  
             </Grid>
          <Grid item md={4}></Grid>
          <Grid item md={4} className="font-weight"><Typography>Supplier</Typography>
             <p>Poorvika Mobiles</p>
             <p>No. 82, Arcot Road, Anna Valaagam Opp: ICICI Bank,</p>
             <p> Kodambakkam,</p>
             <p>Chennai, Tamil Nadu 600024</p>  </Grid>
        </Grid>

        <TableContainer  style={{marginTop:'50px'}}>
            <Table aria-label="simple table">
              <TableHead style={{ padding: '6px!important', background: '#f5f5f5' }}>
                <TableRow className="orderdetails-table">
                  <TableCell align="center" style={{ width: '40%' }}>Items</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Units</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Unit Price</TableCell>
                  <TableCell align="center" style={{ width: '20%' }}>Line Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow className="orderdetails-table1">
                  <TableCell align="center">12T5KI - 12 Ti Xelium Ski"s - 163cm</TableCell>
                  <TableCell align="center">1</TableCell>
                  <TableCell align="center">Rs.200.00</TableCell>
                  <TableCell align="center">Rs.200.00</TableCell>
                </TableRow>
                <TableRow className="orderdetails-table1">
                  <TableCell align="center">12T5KI - 12 Ti Xelium Ski"s - 163cm</TableCell>
                  <TableCell align="center">1</TableCell>
                  <TableCell align="center">Rs.200.00</TableCell>
                  <TableCell align="center">Rs.200.00</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
</Paper>

    </div>
  )
}
